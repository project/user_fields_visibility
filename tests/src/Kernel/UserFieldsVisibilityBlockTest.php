<?php

declare(strict_types=1);

namespace Drupal\Tests\user_fields_visibility\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Tests the User Fields Visibility Block rendering.
 *
 * @group user_fields_visibility
 */
class UserFieldsVisibilityBlockTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'field',
    'field_permissions',
    'system',
    'user',
    'user_fields_visibility',
  ];

  /**
   * The block being tested.
   *
   * @var \Drupal\block\Entity\BlockInterface
   */
  protected $block;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'block', 'user']);
    $this->installSchema('system', ['sequences']);

    $this->installEntitySchema('user');

    $controller = $this->container
      ->get('entity_type.manager')
      ->getStorage('block');
    // Create a block with only required values.
    $this->block = $controller->create([
      'id' => 'userfieldsvisibilityform',
      'theme' => 'bartik',
      'plugin' => 'user_fields_visibility_block',
      'settings' => [
        'id' => 'user_fields_visibility_block',
        'label' => 'User Fields Visibility Form',
        'provider' => 'user_fields_visibility',
        'label_display' => '0',
      ],
    ]);
    $this->block->save();

    $this->container->get('cache.render')->deleteAll();
    $this->renderer = $this->container->get('renderer');

    // Add user field.
    FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'user',
      'field_name' => 'user_fields_visibility_field1',
      'third_party_settings' => [
        'field_permissions' => [
          'permission_type' => 'user_fields_visibility',
        ],
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'user',
      'bundle' => 'user',
      'field_name' => 'user_fields_visibility_field1',
    ])->save();
  }

  /**
   * Tests the rendering of block.
   */
  public function testBlockRendering(): void {
    $storage = $this->container->get('entity_type.manager');

    $user = $this->createUser([
      'set own profile fields visibility',
    ], 'user', FALSE);
    $user->activate();
    $user->save();
    \Drupal::currentUser()->setAccount($user);

    $build = \Drupal::entityTypeManager()
      ->getViewBuilder($this->block->getEntityTypeId())
      ->view($this->block, 'block');

    // Assert access.
    $this->assertTrue($storage->getAccessControlHandler('block')->createAccess($this->block->id(), $user));

    $render = $this->container->get('renderer')->renderRoot($build);
    $crawler = new Crawler($render->__toString());
    $actual = $crawler->filter('form#user-fields-visibility-form');
    $this->assertCount(1, $actual);
    $actual = $crawler->filter('input#edit-visible-fields-user-fields-visibility-field1');
    $this->assertCount(1, $actual);
    $actual = $crawler->filter('label[for="edit-visible-fields-user-fields-visibility-field1"]');
    $this->assertCount(1, $actual);

    // Create a user without permissions.
    $user1 = $this->createUser([], 'user1', FALSE);
    $user1->activate();
    $user1->save();
    \Drupal::currentUser()->setAccount($user1);

    // Assert access.
    $this->assertFalse($storage->getAccessControlHandler('block')->createAccess($this->block->id(), $user1));
  }

}
