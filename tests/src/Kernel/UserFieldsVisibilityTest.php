<?php

declare(strict_types=1);

namespace Drupal\Tests\user_fields_visibility\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests user fields visibility internals.
 *
 * @group user_fields_visibility
 */
class UserFieldsVisibilityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'field',
    'field_permissions',
    'user',
    'user_fields_visibility',
  ];

  /**
   * @covers \Drupal\user_fields_visibility\UserFieldsVisibility::getEligibleFields
   */
  public function testService(): void {
    $this->createFields();
    $fields = $this->container->get('user_fields_visibility')->getEligibleFields();
    $this->assertSame([
      'user_fields_visibility_field1',
      'user_fields_visibility_field2',
    ], array_keys($fields));
  }

  /**
   * Creates testing fields.
   */
  protected function createFields(): void {
    // Field attached to other entity type than user.
    FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'entity_test',
      'field_name' => 'non_user_field',
      'third_party_settings' => [
        'field_permissions' => [
          // Normally, this plugin cannot be configured from UI but we enforce
          // it here via API to check that this field is not exposed to user.
          'permission_type' => 'user_fields_visibility',
        ],
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'non_user_field',
    ])->save();

    // Two user fields using the user_fields_visibility plugin.
    FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'user',
      'field_name' => 'user_fields_visibility_field1',
      'third_party_settings' => [
        'field_permissions' => [
          'permission_type' => 'user_fields_visibility',
        ],
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'user',
      'bundle' => 'user',
      'field_name' => 'user_fields_visibility_field1',
    ])->save();
    FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'user',
      'field_name' => 'user_fields_visibility_field2',
      'third_party_settings' => [
        'field_permissions' => [
          'permission_type' => 'user_fields_visibility',
        ],
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'user',
      'bundle' => 'user',
      'field_name' => 'user_fields_visibility_field2',
    ])->save();

    // A user field using other field permissions plugin.
    FieldStorageConfig::create([
      'type' => 'string',
      'entity_type' => 'user',
      'field_name' => 'custom_field',
      'third_party_settings' => [
        'field_permissions' => [
          'permission_type' => 'custom',
        ],
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'user',
      'bundle' => 'user',
      'field_name' => 'custom_field',
    ])->save();
  }

}
