<?php

declare(strict_types=1);

namespace Drupal\Tests\user_fields_visibility\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the 'user_fields_visibility' module functionality.
 *
 * @group user_fields_visibility
 */
class UserFieldsVisibilityTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'link',
    'user_fields_visibility',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createFields();
  }

  /**
   * Tests access to user fields visibility form.
   */
  public function testFormAccess(): void {
    $assert = $this->assertSession();

    // User with no permissions.
    $account = $this->createUser();
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    // Check that a user not granted with proper perms cannot access the form.
    $assert->statusCodeEquals(403);

    // User with permission but no fields are eligible.
    $account = $this->createUser(['set own profile fields visibility']);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    // Check that a user granted with proper perms cannot access the form as
    // there are no fields using 'user_fields_visibility' plugin.
    $assert->statusCodeEquals(403);

    // User with permission and eligible fields.
    $this->setFieldPermissionsPlugin([
      'string_field' => 'user_fields_visibility',
      'link_field' => 'user_fields_visibility',
    ]);
    $account = $this->createUser(['set own profile fields visibility']);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    // Check that a user granted with proper perms can access the form.
    $assert->statusCodeEquals(200);

    $other_user = $this->createUser();
    $this->drupalLogin($other_user);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    // Check that another user can't access the first user's form.
    $assert->statusCodeEquals(403);

    $other_user = $this->createUser(['set own profile fields visibility']);
    $this->drupalLogin($other_user);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    // Check that another user can't access the first user's form, even they
    // are granted with 'set own profile fields visibility' permission.
    $assert->statusCodeEquals(403);

    $admin = $this->createUser(['administer users']);
    $this->drupalLogin($admin);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    // Check that an admin can always access the other user's form.
    $assert->statusCodeEquals(200);

    // Remove the configurable fields.
    FieldConfig::loadByName('user', 'user', 'string_field')->delete();
    FieldConfig::loadByName('user', 'user', 'link_field')->delete();
    FieldConfig::loadByName('user', 'user', 'integer_field')->delete();

    // Check that form access is denied.
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    $assert->statusCodeEquals(403);
    $this->drupalLogin($admin);
    $this->drupalGet($account->toUrl('fields-visibility-form'));
    $assert->statusCodeEquals(403);
  }

  /**
   * Tests the User fields visibility form.
   */
  public function testVisibilityManagement(): void {
    // Configure all fields to use the 'user_fields_visibility' plugin.
    $this->setFieldPermissionsPlugin([
      'string_field' => 'user_fields_visibility',
      'link_field' => 'user_fields_visibility',
      'integer_field' => 'user_fields_visibility',
    ]);
    $assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $account = $this->createUser(['set own profile fields visibility']);
    $this->drupalLogin($account);
    $this->drupalGet($account->toUrl('fields-visibility-form'));

    // Assert fields are checked by default.
    $assert->checkboxChecked('Field of type string');
    $assert->checkboxChecked('Field of type link');
    $assert->checkboxChecked('Field of type integer');

    $form = $assert->elementExists('css', 'form#user-fields-visibility-form');
    $form->checkField('Field of type string');
    $form->checkField('Field of type link');
    $form->uncheckField('Field of type integer');
    $page->pressButton('Save');
    $assert->checkboxChecked('Field of type string');
    $assert->checkboxChecked('Field of type link');
    $assert->checkboxNotChecked('Field of type integer');

    // Check saving message.
    $assert->pageTextContains('The changes have been saved.');

    // Change visibility.
    $form->uncheckField('Field of type link');
    $form->checkField('Field of type integer');
    $page->pressButton('Save');
    $assert->checkboxChecked('Field of type string');
    $assert->checkboxNotChecked('Field of type link');
    $assert->checkboxChecked('Field of type integer');

    // Assert making an empty selection does not revert
    // to the default behaviour.
    $form->uncheckField('Field of type link');
    $form->uncheckField('Field of type integer');
    $form->uncheckField('Field of type string');
    $page->pressButton('Save');

    $assert->checkboxNotChecked('Field of type string');
    $assert->checkboxNotChecked('Field of type link');
    $assert->checkboxNotChecked('Field of type integer');
  }

  /**
   * Tests the access to fields whose access are managed by this module.
   */
  public function testsFieldAccess(): void {
    $assert = $this->assertSession();

    EntityViewDisplay::create([
      'targetEntityType' => 'user',
      'bundle' => 'user',
      'mode' => 'default',
    ])->setComponent('string_field')
      ->setComponent('link_field')
      ->setComponent('integer_field')
      ->enable()
      ->save();

    $account = $this->createUser([], NULL, FALSE, [
      'string_field' => 'foo',
      'link_field' => 'http://example.com',
      'integer_field' => 1234567890,
      'visible_fields' => serialize([
        'string_field' => 'string_field',
        'integer_field' => 'integer_field',
      ]),
    ]);

    // Check that an admin can always see all the fields.
    $this->drupalLogin($this->createUser(['administer users']));
    $this->drupalGet($account->toUrl());
    $assert->pageTextContains('Field of type string');
    $assert->pageTextContains('foo');
    $assert->pageTextContains('Field of type link');
    $assert->pageTextContains('http://example.com');
    $assert->pageTextContains('Field of type integer');
    $assert->pageTextContains('1234567890');

    // Even the user is exposing two fields, all the fields are displayed
    // because they are not configured with the 'user_fields_visibility' plugin.
    $this->drupalLogin($this->createUser(['access user profiles']));
    $this->drupalGet($account->toUrl());
    $assert->pageTextContains('Field of type string');
    $assert->pageTextContains('foo');
    $assert->pageTextContains('Field of type link');
    $assert->pageTextContains('http://example.com');
    $assert->pageTextContains('Field of type integer');
    $assert->pageTextContains('1234567890');

    // Configure all fields to use the 'user_fields_visibility' plugin.
    $this->setFieldPermissionsPlugin([
      'string_field' => 'user_fields_visibility',
      'link_field' => 'user_fields_visibility',
      'integer_field' => 'user_fields_visibility',
    ]);

    $observer = $this->createUser(['access user profiles'], NULL, FALSE, [
      'string_field' => 'foo',
      'link_field' => 'http://example.com',
      'integer_field' => 1234567890,
    ]);

    // Assert all fields are visible by default.
    $this->drupalGet($observer->toUrl());
    $assert->pageTextContains('Field of type string');
    $assert->pageTextContains('foo');
    $assert->pageTextContains('Field of type link');
    $assert->pageTextContains('http://example.com');
    $assert->pageTextContains('Field of type integer');
    $assert->pageTextContains('1234567890');

    // Only user configured fields are visible.
    $this->drupalGet($account->toUrl());
    $assert->pageTextContains('Field of type string');
    $assert->pageTextContains('foo');
    $assert->pageTextNotContains('Field of type link');
    $assert->pageTextNotContains('http://example.com');
    $assert->pageTextContains('Field of type integer');
    $assert->pageTextContains('1234567890');

    // Edit the user to uncheck all fields.
    $visible_fields = [
      'string_field' => 0,
      'integer_field' => 0,
      'link_field' => 0,
    ];
    $account
      ->set('visible_fields', serialize($visible_fields))
      ->save();

    // Assert no fields are visible.
    $this->drupalLogin($observer);
    $this->drupalGet($account->toUrl());
    $assert->pageTextNotContains('Field of type string');
    $assert->pageTextNotContains('foo');
    $assert->pageTextNotContains('Field of type link');
    $assert->pageTextNotContains('http://example.com');
    $assert->pageTextNotContains('Field of type integer');
    $assert->pageTextNotContains('1234567890');
  }

  /**
   * Creates user configurable fields.
   */
  protected function createFields(): void {
    foreach (['string', 'link', 'integer'] as $type) {
      FieldStorageConfig::create([
        'type' => $type,
        'entity_type' => 'user',
        'field_name' => "{$type}_field",
      ])->save();
      FieldConfig::create([
        'entity_type' => 'user',
        'bundle' => 'user',
        'field_name' => "{$type}_field",
        'label' => "Field of type {$type}",
      ])->save();
    }
  }

  /**
   * Configures certain user fields to use the given field permissions plugins.
   *
   * @param array $fields
   *   An associative array keyed by user field name and having the field
   *   permissions plugin ID as values.
   */
  protected function setFieldPermissionsPlugin(array $fields): void {
    foreach ($fields as $field_name => $plugin_id) {
      /** @var \Drupal\field\FieldStorageConfigInterface $field_storage */
      $field_storage = FieldStorageConfig::loadByName('user', $field_name);
      $field_storage->setThirdPartySetting('field_permissions', 'permission_type', $plugin_id)->save();
    }
  }

}
