# User Fields Visibility

Determines the visibility of the user's profile fields by allowing a user,
granted with proper permissions, to decide which of its own fields are visible
by other users.

## Dependencies

* [Field permissions](https://www.drupal.org/project/field_permissions)

## Usage

* Users granted with `set own profile fields visibility` permission will find a
  new 'Visibility' tab when they are editing their profile.
* The new page exposes a list fields and the user is able to check the fields
  they want to make public. This list contains all user fields configured to use
  the _Configured by user_ field access plugin, from the _Field visibility and
  permissions_ options.

Note that _User Fields Visibility_ will not override Drupal core or other
modules decision to restrict the access to a user profile field.

## Authors

* [Claudiu Cristea](https://www.drupal.org/u/claudiucristea)
* [Andras Szilagyi](https://www.drupal.org/u/andras_szilagyi)
* [Abel Santos](https://www.drupal.org/u/abelcain)
