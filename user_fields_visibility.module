<?php

/**
 * @file
 * Contains user_fields_visibility.module.
 */

declare(strict_types=1);

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user_fields_visibility\Form\UserFieldsVisibilityForm;

/**
 * Implements hook_help().
 */
function user_fields_visibility_help(string $route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.user_fields_visibility':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides management for user fields visibility.') . '</p>';
      $output .= '<h3>' . t('Usage') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t("Adds a new tab on the user's Edit profile screen. The tab contains all managed fields of the user profile.") . '</dt>';
      $output .= '</dl>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function user_fields_visibility_entity_base_field_info(EntityTypeInterface $entity_type): array {
  $fields = [];
  if ($entity_type->id() === 'user') {
    $fields['visible_fields'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Visible fields'))
      ->setDescription(t('Serialized list of visible managed fields.'));
  }
  return $fields;
}

/**
 * Implements hook_entity_type_alter().
 */
function user_fields_visibility_entity_type_alter(array &$entity_types): void {
  /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
  $entity_types['user']
    ->setLinkTemplate('fields-visibility-form', '/user/{user}/visibility')
    ->setFormClass('fields_visibility', UserFieldsVisibilityForm::class);
}
