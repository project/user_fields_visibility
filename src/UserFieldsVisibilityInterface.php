<?php

declare(strict_types=1);

namespace Drupal\user_fields_visibility;

/**
 * Provides an interface for 'user_fields_visibility' service.
 */
interface UserFieldsVisibilityInterface {

  /**
   * Returns a fields list which is subject to user visibility configuration.
   *
   * @return \Drupal\field\FieldConfigInterface[]
   *   A list of field config entities keyed by field name.
   */
  public function getEligibleFields(): array;

}
