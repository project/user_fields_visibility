<?php

declare(strict_types=1);

namespace Drupal\user_fields_visibility\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\user_fields_visibility\UserFieldsVisibilityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to render a User Fields Visibility Form.
 */
class UserFieldsVisibilityForm extends ContentEntityForm {

  /**
   * The user fields visibility service.
   *
   * @var \Drupal\user_fields_visibility\UserFieldsVisibilityInterface
   */
  protected $userFieldsVisibility;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\user_fields_visibility\UserFieldsVisibilityInterface $user_fields_visibility
   *   The user fields visibility service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, UserFieldsVisibilityInterface $user_fields_visibility) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->userFieldsVisibility = $user_fields_visibility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('user_fields_visibility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId(): string {
    // Override the default form base ID just to get rid of the Contact module
    // addition by bypassing contact_form_user_form_alter().
    // @see contact_form_user_form_alter()
    return 'user_fields_visibility_form';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    // Remove standard user form fields.
    $form = array_diff_key($form, array_flip(Element::children($form)));

    $options = array_map(
      function (FieldDefinitionInterface $definition) {
        return $definition->getLabel();
      },
      $this->userFieldsVisibility->getEligibleFields()
    );

    // Override the menu title.
    $form['#title'] = $this->t('Profile fields visibility');

    /** @var \Drupal\Core\Field\FieldItemListInterface $field */
    $field = $this->getEntity()->get('visible_fields');
    $visible_fields = !$field->isEmpty() ? $field->first()->getValue() : [];

    // Grant permission to all fields if the user did not make a selection.
    if (empty($visible_fields)) {
      $visible_fields = array_map(
        function (FieldDefinitionInterface $definition) {
          return $definition->getName();
        },
        $this->userFieldsVisibility->getEligibleFields()
      );
    }

    $form['visible_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select what information should be displayed in your profile'),
      '#options' => $options,
      '#description' => $this->t('Only people who have access to your data will be able to view this information.'),
      '#default_value' => $visible_fields,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('The changes have been saved.'));
    return 0;
  }

}
