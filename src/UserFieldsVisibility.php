<?php

declare(strict_types=1);

namespace Drupal\user_fields_visibility;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Default implementation for 'user_fields_visibility' service.
 */
class UserFieldsVisibility implements UserFieldsVisibilityInterface {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new controller instance.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEligibleFields(): array {
    $configurable_fields = array_filter(
      $this->entityFieldManager->getFieldDefinitions('user', 'user'),
      function (FieldDefinitionInterface $definition): bool {
        return $definition instanceof FieldConfigInterface;
      }
    );

    // Only return 'user_fields_visibility' field permissions type fields.
    return array_filter(
      $configurable_fields,
      function (FieldConfigInterface $definition): bool {
        $field_storage = $definition->getFieldStorageDefinition();
        if ($field_storage instanceof FieldStorageConfigInterface) {
          return $field_storage->getThirdPartySetting('field_permissions', 'permission_type') === 'user_fields_visibility';
        }
        return FALSE;
      }
    );
  }

}
