<?php

declare(strict_types=1);

namespace Drupal\user_fields_visibility\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\user_fields_visibility\UserFieldsVisibilityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Additional, custom access callback for 'entity.user.fields_visibility' route.
 */
class UserFieldsVisibilityController implements ContainerInjectionInterface {

  /**
   * The user fields visibility service.
   *
   * @var \Drupal\user_fields_visibility\UserFieldsVisibilityInterface
   */
  protected $userFieldsVisibility;

  /**
   * Constructs a new controller instance.
   *
   * @param \Drupal\user_fields_visibility\UserFieldsVisibilityInterface $user_fields_visibility
   *   The user fields visibility service.
   */
  public function __construct(UserFieldsVisibilityInterface $user_fields_visibility) {
    $this->userFieldsVisibility = $user_fields_visibility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self($container->get('user_fields_visibility'));
  }

  /**
   * Checks the access to 'entity.user.fields_visibility' route.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user whose field visibility are being edited.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access resolution.
   */
  public function access(UserInterface $user, AccountInterface $account): AccessResultInterface {
    // An administrator.
    $access = AccessResult::allowedIfHasPermission($account, 'administer users');

    if (!$access->isAllowed()) {
      // A user granted with proper permission on their own profile.
      $access = AccessResult::allowedIfHasPermission($account, 'set own profile fields visibility')
        ->andIf(AccessResult::allowedIf($account->id() === $user->id())
      );
    }

    $cache_metadata = (new CacheableMetadata())
      ->addCacheableDependency($user)
      ->addCacheableDependency($account);

    $field_configs = $this->userFieldsVisibility->getEligibleFields();
    foreach ($field_configs as $field_config) {
      $cache_metadata->addCacheableDependency($field_config);
    }

    // Apply collected cache metadata.
    $access->addCacheableDependency($cache_metadata);

    // Allow only if there are configurable fields.
    return $access->andIf(AccessResult::allowedIf(!empty($field_configs)));
  }

}
