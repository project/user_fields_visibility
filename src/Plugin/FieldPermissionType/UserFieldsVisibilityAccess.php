<?php

declare(strict_types=1);

namespace Drupal\user_fields_visibility\Plugin\FieldPermissionType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\field_permissions\Plugin\FieldPermissionType\Base;

/**
 * Defines a user field permission type plugin.
 *
 * @FieldPermissionType(
 *   id = "user_fields_visibility",
 *   title = @Translation("Configured by user"),
 *   description = @Translation("Users are able to set the field visibility in their profile"),
 *   weight = 100,
 * )
 */
class UserFieldsVisibilityAccess extends Base {

  /**
   * {@inheritdoc}
   */
  public function appliesToField(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getTargetEntityTypeId() === 'user';
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldAccess($operation, EntityInterface $entity, AccountInterface $account): bool {
    if ($operation !== 'view') {
      // This plugin acts only for 'view' operation.
      return TRUE;
    }

    if ($account->hasPermission('administer users')) {
      // Administrators are always allowed to see any field of any user.
      return TRUE;
    }

    if ($entity->id() === $account->id()) {
      // A user is always allowed to see its own fields.
      // @todo Reconsider this. What if the site owners want to attach info to
      //   users accessible only by admins. Theoretically, this is possible for
      //   users NOT granted with 'set own profile fields visibility'.
      return TRUE;
    }

    /** @var \Drupal\Core\Field\FieldItemListInterface $field */
    $field = $entity->get('visible_fields');
    $visible_fields = !$field->isEmpty() ? $field->first()->getValue() : [];

    // Fields will be shown by default.
    if (empty($visible_fields)) {
      return TRUE;
    }

    return !empty($visible_fields[$this->fieldStorage->getName()]);
  }

}
